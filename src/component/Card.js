import React from 'react';
import './Cards.css';
import CardItem from './CardItem';

function Cards() {
  return (
    <div className='cards'>
      <h1>Check out favorites food</h1>
      <div className='cards__container'>
        <div className='cards__wrapper'>
          <ul className='cards__items'>
            <CardItem
              src='images/Rendang.jpg'
              text='Delicious Food'
              label='Food'
              path='/services'
            />
            <CardItem
              src='images/Blue-Hawaii,jpg'
              text='Tropical drink'
              label='Luxury Drink'
              path='/services'
            />
          </ul>
          <ul className='cards__items'>
            <CardItem
              src='images/Satay.jpg'
              text='Satay food of Indonesian food'
              label='Food'
              path='/services'
            />
            <CardItem
              src='images/steak.jpg'
              text='Experience our taste of delicious food'
              label='Food'
              path='/products'
            />
            <CardItem
              src='images/Tomato-juice.jpg'
              text='drink juice'
              label='Juice'
              path='/sign-up'
            />
          </ul>
        </div>
      </div>
    </div>
  );
}

export default Cards;